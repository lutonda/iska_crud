
--
-- Table structure for table `Clientes`
--

CREATE TABLE `Clientes` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(50) DEFAULT NULL,
  `NumeroBi` varchar(50) DEFAULT NULL,
  `ContabancariaId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ;

--
-- Table structure for table `ContaBancarias`
--

CREATE TABLE `ContaBancarias` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Numero` int(11) DEFAULT NULL,
  `Saldo` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ;
