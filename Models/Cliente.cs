using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace crudmysql.Models
{
    public class Cliente
    {
        
        [ForeignKey("Contabancaria")]
        public int ContabancariaId { get; set; }
        
        public int Id { get; set; }
        public string Nome { get; set; }
        public string NumeroBi { get; set; }
        public virtual ContaBancaria Contabancaria { get; set; }
    }
}