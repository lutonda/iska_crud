
using Microsoft.EntityFrameworkCore;

namespace crudmysql.Models
{
    public class ContaBancariaContexto: DbContext
    {
        public ContaBancariaContexto(DbContextOptions<ContaBancariaContexto> options) : base(options)
        {
        }
        public DbSet<ContaBancaria> ContaBancarias { get; set; }
        
    }
}