using Microsoft.EntityFrameworkCore;

namespace crudmysql.Models
{
    public class ClienteContexto: DbContext
        {
            public ClienteContexto(DbContextOptions<ClienteContexto> options) : base(options)
            {
            }
            public DbSet<Cliente> Clientes { get; set; }
            public DbSet<ContaBancaria> ContaBancarias { get; set; }
        
    }
}