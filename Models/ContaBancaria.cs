

using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace crudmysql.Models
{
    public class ContaBancaria
    {
        
        [ForeignKey("Cliente")]
        
        public int Id { get; set; }
        public int Numero { get; set; }
        public int Saldo { get; set; }
    }
}